#language: pt

Funcionalidade: Receber pedido de locaçao
    Sendo um anunciante que possui equipamentos cadastrados
    Desejo receber pedidos de locação
    Para que eu possa decidir se quero aprová-los ou rejeitá-los

    Cenário: Receber pedido

    Dado que meu perfil de anunciante é "nat@gmail.com" e "debs123"
        E que eu tenho o seguinte equipamento cadastrado:
        | thumb     | trompete.jpg |
        | nome      | Trompete     |
        | categoria | Outros       |
        | preco     | 100          |
        E acesso o meu dashboard
    Quando "joao@gmail.com" e "debs123" solicita a locação desse equipo
    Então devo ver a seguinte mensagem: 
        """
        joao@gmail.com deseja alugar o equipamento: Trompete em: DATA_ATUAL
        """
        E devo ver os links: "ACEITAR" e "REJEITAR" no pedido
